import 'package:flutter/material.dart';
import 'package:audioplayers/audioplayers.dart';

void main() {
  runApp(MaterialApp(
    home: SafeArea(
      child: Scaffold(
        body: MusicPlayer(),
      ),
    ),
  ));
}

class MusicPlayer extends StatefulWidget {
  const MusicPlayer({Key? key}) : super(key: key);

  @override
  State<MusicPlayer> createState() => _MusicPlayerState();
}

class _MusicPlayerState extends State<MusicPlayer> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          getButton('1', Colors.blue),
          getButton('2', Colors.orange),
          getButton('3', Colors.yellow),
          getButton('4', Colors.pink),
          getButton('5', Colors.indigo),
          getButton('6', Colors.teal),
          getButton('7', Colors.black),
        ],
      ),
    );
  }

  ElevatedButton getButton(String no, Color? color) {
    return ElevatedButton(onPressed: () async {
      final player = AudioPlayer();
      await player.setSource(AssetSource('assets_note$no.wav'));
      await player.resume();
    },
    style: ElevatedButton.styleFrom(backgroundColor: color),
    child: Text('Button $no'));
  }
}
