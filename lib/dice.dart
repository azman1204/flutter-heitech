import 'package:flutter/material.dart';
import 'dadu2.dart';

void main() {
  runApp(MaterialApp(
    home: Scaffold(
      backgroundColor: Colors.teal,
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text('LAMBUNG DADU'),
      ),
      body: Dadu2(), // ini stateless widget
    ),
  ));
}