import 'package:flutter/material.dart';
import 'dart:math';

class Dadu extends StatelessWidget {
  int number1 = 1;
  int number2 = 1;
  //const Dadu({Key? key}) : super(key: key);

  @override
  // semua widget dlm function ini boleh hot reload
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                child: Image.asset('images/dice$number1.png'),
                onPressed: () {
                  int number1 = Random().nextInt(6) + 1;
                  print('you clicked me $number1');
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                child: Image.asset('images/dice$number2.png'),
                onPressed: () {},
              ),
            ),
          ),
        ],
      ),
    );
  }
}
