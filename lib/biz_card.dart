import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    home: SafeArea(
      child: Scaffold(
        backgroundColor: Colors.teal.shade900,
        body: Padding(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const CircleAvatar(
                radius: 50,
                backgroundImage: AssetImage('images/keanu.png'),
              ),
              const Text(
                'Azman bin Zakaria',
                style: TextStyle(
                  fontFamily: 'Freehand',
                  fontSize: 45.0,
                  color: Colors.white,
                ),
              ),
              const Text(
                'Mobile App Developer',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25
                ),
              ),
              const SizedBox(width: 240.0, height: 20.0,child: Divider(color: Colors.white,),),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: const [
                      Icon(Icons.email, color: Colors.teal),
                      SizedBox(width: 8.0,),
                      Text('azman120474@gmail.com'),
                    ],
                  ),
                ),
              ),
              Card(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: const [
                      Icon(Icons.phone, color: Colors.teal,),
                      SizedBox(width: 8.0,),
                      Text('+016 2370 394'),
                    ],
                  ),
                ),
              )

            ],
          ),
        ),
      ),
    ),
  ));
}
