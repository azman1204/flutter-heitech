//gitlab.com/azman1204
import 'package:flutter/material.dart';
import 'dart:math';

class Dadu2 extends StatefulWidget {
  const Dadu2({Key? key}) : super(key: key);

  @override
  State<Dadu2> createState() => _Dadu2State();
}

class _Dadu2State extends State<Dadu2> {
  int number1 = 1;
  int number2 = 1;
  //const Dadu({Key? key}) : super(key: key);

  @override
  // semua widget dlm function ini boleh hot reload
  Widget build(BuildContext context) {
    return Center(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                child: Image.asset('images/dice$number1.png'),
                onPressed: () {
                  print('you clicked me $number1');
                  // refresh yg berubah shj
                  setState(() {
                    number1 = Random().nextInt(6) + 1;
                    number2 = Random().nextInt(6) + 1;
                  });
                },
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextButton(
                child: Image.asset('images/dice$number2.png'),
                onPressed: () {
                  setState(() {
                    number1 = Random().nextInt(6) + 1;
                    number2 = Random().nextInt(6) + 1;
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
